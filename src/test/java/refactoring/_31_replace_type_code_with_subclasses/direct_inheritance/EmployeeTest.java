package refactoring._31_replace_type_code_with_subclasses.direct_inheritance;

import org.junit.jupiter.api.Test;
import refactoring._31_replace_type_code_with_subclasses.after.direct_inheritance.Employee;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void employeeType() {
        assertEquals("engineer", Employee.createEmployee("hakyong", "engineer").getType());
        assertEquals("manager", Employee.createEmployee("hayoung", "manager").getType());
        assertThrows(IllegalArgumentException.class, () -> Employee.createEmployee("keesun", "wrong type"));
    }

}