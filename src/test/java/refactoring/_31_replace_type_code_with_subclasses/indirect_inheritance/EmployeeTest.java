package refactoring._31_replace_type_code_with_subclasses.indirect_inheritance;

import org.junit.jupiter.api.Test;
import refactoring._31_replace_type_code_with_subclasses.after.indirect_inheritance.Employee;
import refactoring._31_replace_type_code_with_subclasses.after.indirect_inheritance.FullTimeEmployee;
import refactoring._31_replace_type_code_with_subclasses.after.indirect_inheritance.PartTimeEmployee;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class EmployeeTest {

    @Test
    void capitalizedType() {
        assertEquals("Manager", new FullTimeEmployee("keesun", "manager").capitalizedType());
        assertEquals("Engineer", new PartTimeEmployee("keesun", "engineer").capitalizedType());
        assertThrows(IllegalArgumentException.class, () -> new Employee("keesun", "wrong type"));
    }

}