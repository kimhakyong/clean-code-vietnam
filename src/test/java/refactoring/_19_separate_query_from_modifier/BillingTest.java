package refactoring._19_separate_query_from_modifier;

import org.junit.jupiter.api.Test;
import refactoring._19_separate_query_from_modifier.before.billing.Billing;
import refactoring._19_separate_query_from_modifier.before.billing.Customer;
import refactoring._19_separate_query_from_modifier.before.billing.EmailGateway;
import refactoring._19_separate_query_from_modifier.before.billing.Invoice;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class BillingTest {

    @Test
    void totalOutstanding() {
        Billing billing = new Billing(
                new Customer("harvey",
                        List.of(new Invoice(20), new Invoice(30))),
                new EmailGateway());
        assertEquals(50d, billing.getTotalOutstandingAndSendBill());
    }
}