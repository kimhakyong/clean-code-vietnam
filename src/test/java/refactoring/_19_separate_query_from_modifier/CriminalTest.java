package refactoring._19_separate_query_from_modifier;

import org.junit.jupiter.api.Test;
import refactoring._19_separate_query_from_modifier.before.criminal.Criminal;
import refactoring._19_separate_query_from_modifier.before.criminal.Person;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CriminalTest {

    @Test
    void alertForMiscreant() {
        Criminal criminal = new Criminal();
        String found = criminal.alertForMiscreant(List.of(new Person("Harvey"), new Person("Adam")));
        assertEquals("", found);

        found = criminal.alertForMiscreant(List.of(new Person("John"), new Person("Don")));
        assertEquals("John", found);
    }

}