package refactoring._37_hide_delegate;

import org.junit.jupiter.api.Test;
import refactoring._37_hide_delegate.before.Department;
import refactoring._37_hide_delegate.before.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonTest {

    @Test
    void manager() {
        Person harvey = new Person("harvey");
        harvey.setDepartment(new Department("m365deploy", "LA", "nick"));

        Department department = harvey.getDepartment();
        assertEquals("nick", department.getManager());
    }
}