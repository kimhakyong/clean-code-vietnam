package refactoring._22_combine_functions_into_transform;

import org.junit.jupiter.api.Test;
import refactoring._22_combine_functions_into_transform.before.Client1;
import refactoring._22_combine_functions_into_transform.before.Client2;
import refactoring._22_combine_functions_into_transform.before.Client3;
import refactoring._22_combine_functions_into_transform.before.Reading;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ReadingClientTest {
    @Test
    public void client1() {
        Client1 client1 = new Client1(new Reading(5, 2));
        assertEquals(10d, client1.getBaseCharge());
    }

    @Test
    public void client2() {
        Client2 client2 = new Client2(new Reading(2, 10));
        assertEquals(20d, client2.getBaseCharge());
        assertEquals(22d, client2.getTaxCharge());
    }

    @Test
    public void client3() {
        Client3 client3 = new Client3(new Reading(10, 3));
        assertEquals(30d, client3.getBasicChargeAmount());
    }
}
