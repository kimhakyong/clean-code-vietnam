package refactoring._21_replace_derived_variable_with_query;

import org.junit.jupiter.api.Test;
import refactoring._21_replace_derived_variable_with_query.before.discount.Discount;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DiscountTest {

    @Test
    void discount() {
        Discount discount = new Discount(100);
        discount.setDiscount(10);
        assertEquals(90, discount.getDiscountedTotal());

        discount.setBaseTotal(200);
        discount.setDiscount(10);
        assertEquals(190, discount.getDiscountedTotal());

        discount.setDiscount(30);
        assertEquals(170, discount.getDiscountedTotal());
    }

}