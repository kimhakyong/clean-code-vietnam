package refactoring._38_remove_middle_man;

import org.junit.jupiter.api.Test;
import refactoring._38_remove_middle_man.before.Department;
import refactoring._38_remove_middle_man.before.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PersonTest {
    @Test
    void getManager() {
        Person harvey = new Person("harvey");
        harvey.setDepartment(new Department("sales team", "LA", "nick"));
        assertEquals("nick", harvey.getDepartmentManager());
    }
}