package basic_junit.base;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.condition.DisabledOnOs;
import org.junit.jupiter.api.condition.EnabledIfEnvironmentVariable;
import org.junit.jupiter.api.condition.OS;
import org.junit.jupiter.api.extension.RegisterExtension;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;

import java.time.Duration;
import java.util.function.Supplier;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.junit.jupiter.api.Assumptions.assumingThat;

//@ExtendWith(FindSlowTestExtension.class)
@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
@TestInstance(TestInstance.Lifecycle.PER_CLASS) // PER_METHOD
@TestMethodOrder(MethodOrderer.OrderAnnotation.class) // Alpahanumeric / Random
public class StudyTest {
    @RegisterExtension
    static FindSlowTestExtension findSlowTestExtension = new FindSlowTestExtension(1000L);

    int instanceValue = 0;

    @BeforeAll
    static void before_all() {
        System.out.println("Before All");
    }

    @BeforeEach
    void before_each() {
        System.out.println("Before Each");
    }

    @Test
    @DisplayName("프로세스 테스트1")
    void process_test1() {
        System.out.println("process Test1");
    }

    @Test
    @DisplayName("프로세스 테스트2 🥰😊")
    void process_test2() {
        System.out.println("process Test2");
    }

    @Test
    void various_tests_for_study() {
        Study study = new Study(StudyStatus.DRAFT, 10);
        assertNotNull(study);
        assertEquals(StudyStatus.DRAFT, study.getStatus(), "The study status value must be draft.");
        assertEquals(StudyStatus.DRAFT, study.getStatus(), new Supplier<String>() {
            @Override
            public String get() {
                return "The study status value must be draft.";
            }
        });
        assertEquals(StudyStatus.DRAFT, study.getStatus(), () -> "The study status value must be draft.");

        assertAll(
                // assertEqauls : 값이 같은지
                // assertSame : 동일한 객체인지
                () -> assertSame(study.getStatus(), StudyStatus.DRAFT),
                () -> assertTrue(study.getLimit() > 0)
        );

        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class,
                () -> new Study(StudyStatus.DRAFT, -1));
        assertEquals("The limit value must be greater than 0.", exception.getMessage());

        assertTimeout(Duration.ofSeconds(1), () -> {
            new Study(StudyStatus.DRAFT, 10);
            Thread.sleep(100);
        });

        assertTimeoutPreemptively(Duration.ofSeconds(1), () -> {
            new Study(StudyStatus.DRAFT, 10);
            Thread.sleep(100);
        });
    }

    @Test
//    @EnabledOnOs({OS.WINDOWS, OS.MAC, OS.LINUX})
    @DisabledOnOs({OS.WINDOWS, OS.MAC, OS.LINUX})
    @EnabledIfEnvironmentVariable(named = "TEST_ENV", matches = "LOCAL")
    void run_tests_based_on_conditions() {
        assumeTrue("LOCAL".equalsIgnoreCase(System.getenv("TEST_ENV")));

        Study study = new Study(StudyStatus.DRAFT, 10);
        assertNotNull(study);

        assumingThat("PROD".equalsIgnoreCase(System.getenv("TEST_ENV")), () -> {
            assertEquals(StudyStatus.DRAFT, study.getStatus());
            assertEquals(10, study.getLimit());
        });
    }

    @Test
    @Tag("LOCAL")
    void tested_only_on_LOCAL() {
        System.out.println("Tested only on LOCAL");
    }

    @Test
    @Tag("PROD")
    void tested_only_on_PROD() {
        System.out.println("Tested only on PROD");
    }

    @ProdAnnotation
    void test_using_custom_annotation_by_prod_annotation() {
        System.out.println("Prod Annotation");
    }

    @LocalAnnotation
    void test_using_custom_annotation_by_local_annotation() {
        System.out.println("Local Annotation");
    }

    @DisplayName("Repetition Test")
    @RepeatedTest(value = 5, name = "{displayName}, {currentRepetition} / {totalRepetitions}")
//    @RepeatedTest(10)
    void repeat_the_test(RepetitionInfo repetitionInfo) {
        System.out.println("Repeated Test : "
                + repetitionInfo.getCurrentRepetition()
                + " / "
                + repetitionInfo.getTotalRepetitions());
    }

    @ParameterizedTest
    @ValueSource(strings = {"one", "two", "three"})
//    @EmptySource
//    @NullSource
    @NullAndEmptySource
    void parameterized_test_using_string(String param) {
        System.out.println(param);
    }

    @DisplayName("Parameterized Test")
    @ParameterizedTest(name = "{index} {displayName} Param={0}")
    @ValueSource(ints = {1, 3, 5})
    void parameterized_test_using_integer(int param) {
        System.out.println(param);
    }

    @ParameterizedTest
    @CsvSource({"'one', 1", "'two', 2", "'three', 3"})
    void parameterized_test_using_csv(String param1, int param2) {
        System.out.println(param1 + " : " + param2);
    }

    @DisplayName("second order test")
    @Test
    @Order(2)
    void test_instance_first() {
        this.instanceValue++;
        System.out.println("instance hash value : " + this);
        System.out.println("instance value : " + instanceValue);
    }

    @DisplayName("first order test")
    @Test
    @Order(1)
    void test_instance_second() throws InterruptedException {
        this.instanceValue++;
        System.out.println("instance hash value : " + this);
        System.out.println("instance value : " + instanceValue);

        Thread.sleep(1005L);
    }

    @AfterEach
    void after_each() {
        System.out.println("After Each");
    }

    @AfterAll
    static void after_all() {
        System.out.println("After All");
    }
}
