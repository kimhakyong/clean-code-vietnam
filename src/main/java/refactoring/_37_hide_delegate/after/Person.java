package refactoring._37_hide_delegate.after;

public class Person {

    private String name;

    private Department department;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    private Department getDepartment() {
        return department;
    }

    public String getDepartmentManager() {
        return getDepartment().getManager();
    }
}
