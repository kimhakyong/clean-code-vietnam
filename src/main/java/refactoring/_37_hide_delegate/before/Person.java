package refactoring._37_hide_delegate.before;

// Practice
// Do not return Department directly.
// Access and process the Department internal fields from Person.

public class Person {
    private String name;

    private Department department;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
