package refactoring._38_remove_middle_man.before;

public class Department {
    private String name;
    private String location;
    private String manager;

    public Department(String name, String location, String manager) {
        this.name = name;
        this.location = location;
        this.manager = manager;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getManager() {
        return manager;
    }
}
