package refactoring._38_remove_middle_man.before;

// Practice
// When you need many properties of the delegate class
// It is efficient to return the delegate class directly.

public class Person {
    private String name;
    private Department department;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getDepartmentName() {
        return this.department.getName();
    }

    public String getDepartmentLocation() {
        return this.department.getLocation();
    }

    public String getDepartmentManager() {
        return this.department.getManager();
    }
}
