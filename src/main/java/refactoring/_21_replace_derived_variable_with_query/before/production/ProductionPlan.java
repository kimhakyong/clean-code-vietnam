package refactoring._21_replace_derived_variable_with_query.before.production;

import java.util.ArrayList;
import java.util.List;

// Practice
// Calculate and return derived variables when necessary.
// Derived variables : production

public class ProductionPlan {

    private double production;
    private List<Double> adjustments = new ArrayList<>();

    public void applyAdjustment(double adjustment) {
        this.adjustments.add(adjustment);
        this.production += adjustment;
    }

    public double getProduction() {
        return this.production;
    }
}
