package refactoring._21_replace_derived_variable_with_query.after.production;

import java.util.ArrayList;
import java.util.List;

public class ProductionPlan {

    private double production;
    private List<Double> adjustments = new ArrayList<>();

    public void applyAdjustment(double adjustment) {
        this.adjustments.add(adjustment);
    }

    public double getProduction() {
//        return this.adjustments.stream().reduce((double) 0, (a, b) -> a + b);
        return this.adjustments.stream().reduce((double) 0, Double::sum);
    }
}
