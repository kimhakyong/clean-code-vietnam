package refactoring._18_split_variable.after;

public class Order {

    public double discount(double inputValue, int quantity) {
        double result = inputValue;

        if (inputValue > 50) result = result - 2;
        if (quantity > 100) result = result - 1;

        return result;
    }
}
