package refactoring._22_combine_functions_into_transform.before;

// Practice
// Create a new PayReading class and move baseCharge.

public class Client1 {
    private double baseCharge;

    public Client1(Reading reading) {
        this.baseCharge = reading.getSubscriptionMonths() * reading.getQuantity();
    }

    public double getBaseCharge() {
        return baseCharge;
    }
}
