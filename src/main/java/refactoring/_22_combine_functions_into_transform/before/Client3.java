package refactoring._22_combine_functions_into_transform.before;

public class Client3 {
    private double basicChargeAmount;

    public Client3(Reading reading) {
        this.basicChargeAmount = calculateBaseCharge(reading);
    }

    private double calculateBaseCharge(Reading reading) {
        return reading.getSubscriptionMonths() * reading.getQuantity();
    }

    public double getBasicChargeAmount() {
        return basicChargeAmount;
    }
}
