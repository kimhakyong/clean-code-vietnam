package refactoring._22_combine_functions_into_transform.after;

public class Client3 {
    private double basicChargeAmount;

    public Client3(Reading reading) {
        this.basicChargeAmount = reading.calculateBaseCharge();
    }

    public double getBasicChargeAmount() {
        return basicChargeAmount;
    }
}
