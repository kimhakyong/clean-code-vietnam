package refactoring._22_combine_functions_into_transform.after;

public class Client2 {

    private double baseCharge;
    private double taxCharge;

    public Client2(Reading reading) {
        this.baseCharge = reading.calculateBaseCharge();
        this.taxCharge = this.baseCharge * 0.1;
    }

    public double getBaseCharge() {
        return baseCharge;
    }

    public double getTaxCharge() {
        return taxCharge;
    }
}
