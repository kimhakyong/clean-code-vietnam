package refactoring._22_combine_functions_into_transform.after;

public class Client1 {
    private double baseCharge;

    public Client1(Reading reading, PayReading payReading) {
        this.baseCharge = payReading.calculateBaseCharge();
    }

    public double getBaseCharge() {
        return baseCharge;
    }
}
