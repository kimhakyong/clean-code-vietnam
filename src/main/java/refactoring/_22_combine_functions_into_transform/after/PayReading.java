package refactoring._22_combine_functions_into_transform.after;

public class PayReading {
    private final Reading reading;

    public PayReading(Reading reading) {
        this.reading = reading;
    }

    public double calculateBaseCharge() {
        return this.reading.getSubscriptionMonths() * this.reading.getQuantity();
    }

    public double calculateTaxCharge() {
        double baseCharge = this.calculateBaseCharge();
        return baseCharge + baseCharge * 0.1;
    }
}
