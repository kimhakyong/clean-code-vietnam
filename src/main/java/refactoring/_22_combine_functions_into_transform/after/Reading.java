package refactoring._22_combine_functions_into_transform.after;

//public record Reading(String customer, double quantity, Month month, Year year) {
//}

public class Reading {
    private final int quantity;
    private final int subscriptionMonths;

    public Reading(int quantity, int subscriptionMonths) {
        this.quantity = quantity;
        this.subscriptionMonths = subscriptionMonths;
    }

    public int getQuantity() {
        return quantity;
    }

    public int getSubscriptionMonths() {
        return subscriptionMonths;
    }

    public double calculateBaseCharge() {
        return this.getSubscriptionMonths() * this.getQuantity();
    }
}
