package refactoring._10_data_clumps.after;

public class Employee {

    private String name;

    private TelephoneNumber telephoneNumber;

    public Employee(String name, TelephoneNumber telephoneNumber) {
        this.name = name;
        this.telephoneNumber = telephoneNumber;
    }

    public String personalPhoneNumber() {
        return telephoneNumber.toString();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPersonalAreaCode() {
        return telephoneNumber.getAreaCode();
    }

    public void setPersonalAreaCode(String personalAreaCode) {
        telephoneNumber.setAreaCode(personalAreaCode);
    }

    public String getPersonalNumber() {
        return telephoneNumber.getNumber();
    }

    public void setPersonalNumber(String personalNumber) {
        telephoneNumber.setNumber(personalNumber);
    }
}
