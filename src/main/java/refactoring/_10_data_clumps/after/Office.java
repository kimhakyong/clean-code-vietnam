package refactoring._10_data_clumps.after;

public class Office {
    private String location;

    private TelephoneNumber officePhoneNumber;

    public Office(String location, TelephoneNumber officePhoneNumber) {
        this.location = location;
        this.officePhoneNumber = officePhoneNumber;
    }

    public String officePhoneNumber() {
        return officePhoneNumber.toString();
    }

    public String getOfficeAreaCode() {
        return officePhoneNumber.getAreaCode();
    }

    public void setOfficeAreaCode(String officeAreaCode) {
        officePhoneNumber.setAreaCode(officeAreaCode);
    }

    public String getOfficeNumber() {
        return officePhoneNumber.getNumber();
    }

    public void setOfficeNumber(String officeNumber) {
        officePhoneNumber.setNumber(officeNumber);
    }
}
