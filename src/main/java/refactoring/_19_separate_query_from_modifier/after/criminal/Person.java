package refactoring._19_separate_query_from_modifier.after.criminal;

public class Person {

    private String name;

    public Person(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }
}
