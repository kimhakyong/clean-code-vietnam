package refactoring._19_separate_query_from_modifier.before.billing;

public class EmailGateway {
    public void send(String bill) {
        System.out.println(bill);
    }
}
