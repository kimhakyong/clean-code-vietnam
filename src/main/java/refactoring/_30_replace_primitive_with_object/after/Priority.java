package refactoring._30_replace_primitive_with_object.after;

import java.util.List;

public class Priority {
    private String value;
    private List<String> legalValues = List.of("low", "normal", "high", "rush");

    public Priority(String value) {
        // type safe
        if (legalValues.contains(value))
            this.value = value;
        else
            throw new IllegalArgumentException("illegal value for priority " + value);
    }

    @Override
    public String toString() {
        return this.value;
    }

    private int index() {
        return this.legalValues.indexOf(this.value);
    }

    public boolean higherThen(Priority other) {
        return this.index() > other.index();
    }
}
