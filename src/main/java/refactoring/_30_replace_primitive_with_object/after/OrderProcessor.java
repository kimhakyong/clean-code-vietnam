package refactoring._30_replace_primitive_with_object.after;

import java.util.List;

public class OrderProcessor {

    public long numberOfHighPriorityOrders(List<Order> orders) {
        return orders.stream()
                .filter(o -> o.getPriority().toString().equals("high")
                        || o.getPriority().toString().equals("rush"))
                .count();
//        return 0L;
    }
}
