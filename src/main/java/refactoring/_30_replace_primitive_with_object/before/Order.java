package refactoring._30_replace_primitive_with_object.before;

// Practice
// Change the priority field to Priority class.

public class Order {

    private String priority;

    public Order(String priority) {
        this.priority = priority;
    }

    public String getPriority() {
        return priority;
    }
}
