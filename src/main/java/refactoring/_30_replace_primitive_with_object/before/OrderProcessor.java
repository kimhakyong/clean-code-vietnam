package refactoring._30_replace_primitive_with_object.before;

import java.util.List;

public class OrderProcessor {

    // "low", "normal", "high", "rush"
    public long numberOfHighPriorityOrders(List<Order> orders) {
        return orders.stream()
                .filter(o -> o.getPriority() == "high" || o.getPriority() == "rush")
                .count();
    }
}
