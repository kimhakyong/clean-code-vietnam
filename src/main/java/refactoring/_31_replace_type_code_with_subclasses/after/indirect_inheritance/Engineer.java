package refactoring._31_replace_type_code_with_subclasses.after.indirect_inheritance;

public class Engineer extends EmployeeType {
    @Override
    public String toString() {
        return "engineer";
    }
}
