package refactoring._31_replace_type_code_with_subclasses.before.indirect_inheritance;

import java.util.List;

// Practice
// Create a new EmployeeType for the “engineer”, “manager”, and “salesman” types and define them in the form of subclasses.
// Change Employee's type field to delegate type using EmployeeType.

public class Employee {

    private String name;

    private String type;

    public Employee(String name, String type) {
        this.validate(type);
        this.name = name;
        this.type = type;
    }

    private void validate(String type) {
        List<String> legalTypes = List.of("engineer", "manager", "salesman");
        if (!legalTypes.contains(type)) {
            throw new IllegalArgumentException(type);
        }
    }

    public String capitalizedType() {
        return this.type.substring(0, 1).toUpperCase() + this.type.substring(1).toLowerCase();
    }

    @Override
    public String toString() {
        return "Employee{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
