package refactoring._31_replace_type_code_with_subclasses.before.indirect_inheritance;

public class FullTimeEmployee extends Employee {
    public FullTimeEmployee(String name, String type) {
        super(name, type);
    }
}
