package refactoring._32_replace_conditional_with_polymorphism.after;

public class FullTimeEmployee extends Employee {
    public FullTimeEmployee() {
    }

    @Override
    public int vacationHours() {
        return 120;
    }

    @Override
    public boolean canAccessTo(String project) {
        return true;
    }
}
