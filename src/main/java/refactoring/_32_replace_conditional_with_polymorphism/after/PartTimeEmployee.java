package refactoring._32_replace_conditional_with_polymorphism.after;

import java.util.List;

public class PartTimeEmployee extends Employee {
    public PartTimeEmployee(List<String> availableProjects) {
        super(availableProjects);
    }

    @Override
    public int vacationHours() {
        return 80;
    }
}
