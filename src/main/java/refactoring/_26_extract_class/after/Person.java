package refactoring._26_extract_class.after;

public class Person {
    private TelephoneNumber telephoneNumber;
    private String name;

    public Person(TelephoneNumber telephoneNumber, String name) {
        this.telephoneNumber = telephoneNumber;
        this.name = name;
    }

    public String telephoneNumber() {
        return this.telephoneNumber.toString();
    }

    public String name() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String officeAreaCode() {
        return telephoneNumber.getAreaCode();
    }

    public void setOfficeAreaCode(String officeAreaCode) {
        telephoneNumber.setAreaCode(officeAreaCode);
    }

    public String officeNumber() {
        return telephoneNumber.getNumber();
    }

    public void setOfficeNumber(String officeNumber) {
        telephoneNumber.setNumber(officeNumber);
    }
}
