package refactoring._24_split_phase.after;

public class PriceData {
    private final double basePrice;
    private final double discount;
    private final int quantity;

    public PriceData(double basePrice, double discount, int quantity) {
        this.basePrice = basePrice;
        this.discount = discount;
        this.quantity = quantity;
    }

    public double basePrice() {
        return basePrice;
    }

    public double discount() {
        return discount;
    }

    public int quantity() {
        return quantity;
    }
}
