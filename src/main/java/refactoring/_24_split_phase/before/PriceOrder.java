package refactoring._24_split_phase.before;

// Practice
// Split the priceOrder method.
// Create and use the PriceData class to store intermediate values when dividing.

public class PriceOrder {
    public double priceOrder(Product product, int quantity, ShippingMethod shippingMethod) {
        final double basePrice = product.basePrice() * quantity;

        final double discount = Math.max(quantity - product.discountThreshold(), 0)
                * product.basePrice() * product.discountRate();

        final double shippingPerCase = (basePrice > shippingMethod.discountThreshold()) ?
                shippingMethod.discountedFee() : shippingMethod.feePerCase();

        final double shippingCost = quantity * shippingPerCase;

        final double price = basePrice - discount + shippingCost;
        return price;
    }
}
