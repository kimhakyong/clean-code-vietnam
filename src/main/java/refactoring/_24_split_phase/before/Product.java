package refactoring._24_split_phase.before;

//public record Product(double basePrice, double discountThreshold, double discountRate) {
//}

public class Product {
    private final double basePrice;
    private final double discountThreshold;
    private final double discountRate;

    public Product(double basePrice, double discountThreshold, double discountRate) {
        this.basePrice = basePrice;
        this.discountThreshold = discountThreshold;
        this.discountRate = discountRate;
    }

    public double basePrice() {
        return basePrice;
    }

    public double discountThreshold() {
        return discountThreshold;
    }

    public double discountRate() {
        return discountRate;
    }
}
