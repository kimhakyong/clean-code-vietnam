package refactoring._24_split_phase.before;

//public record ShippingMethod(double discountThreshold, double discountedFee, double feePerCase) {
//}

public class ShippingMethod {
    private final double discountThreshold;
    private final double discountedFee;
    private final double feePerCase;

    public ShippingMethod(double discountThreshold, double discountedFee, double feePerCase) {
        this.discountThreshold = discountThreshold;
        this.discountedFee = discountedFee;
        this.feePerCase = feePerCase;
    }

    public double discountThreshold() {
        return discountThreshold;
    }

    public double discountedFee() {
        return discountedFee;
    }

    public double feePerCase() {
        return feePerCase;
    }
}