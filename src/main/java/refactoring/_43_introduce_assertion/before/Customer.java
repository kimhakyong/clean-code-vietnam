package refactoring._43_introduce_assertion.before;

// Practice
// Change the validation check using the java assert statement.

public class Customer {

    private Double discountRate;

    public double applyDiscount(double amount) {
        return (this.discountRate < 1.0) ? amount - (this.discountRate * amount) : amount;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Double discountRate) {
        this.discountRate = discountRate;
    }
}
