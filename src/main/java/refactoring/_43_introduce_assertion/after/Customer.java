package refactoring._43_introduce_assertion.after;

public class Customer {

    private Double discountRate;

    public double applyDiscount(double amount) {
        return (this.discountRate < 1.0) ? amount - (this.discountRate * amount) : amount;
    }

    public Double getDiscountRate() {
        return discountRate;
    }

    public void setDiscountRate(Double discountRate) {
        assert discountRate > 0.0 && discountRate < 1.0 : "discountRate must be greater than 1.0.";
        if (discountRate < 0.0 || discountRate >= 1.0) {
            throw new IllegalArgumentException(String.valueOf(discountRate));
        }
        this.discountRate = discountRate;
    }
}
