package refactoring._15_remove_flag_argument.after;

import java.time.LocalDate;

public class Shipment {

    public LocalDate deliveryDate(Order order, boolean isRush) {
        if (isRush) {
            return rushDeliveryDate(order);
        } else {
            return regularDeliveryDate(order);
        }
    }

    public LocalDate regularDeliveryDate(Order order) {
        int deliveryTime = 0;
        switch (order.getDeliveryState()) {
            case "WA":
            case "CA":
            case "OR":
                deliveryTime = 1;
                break;
            case "TX":
            case "NY":
            case "FL":
                deliveryTime = 2;
                break;
            default:
                deliveryTime = 3;
        }

        return order.getPlacedOn().plusDays(deliveryTime);
    }

    public LocalDate rushDeliveryDate(Order order) {
        int deliveryTime = 0;
        switch (order.getDeliveryState()) {
            case "WA":
            case "CA":
                deliveryTime = 2;
                break;
            case "OR":
            case "TX":
            case "NY":
                deliveryTime = 3;
                break;
            default:
                deliveryTime = 4;
        }

        return order.getPlacedOn().plusDays(deliveryTime);
    }
}
