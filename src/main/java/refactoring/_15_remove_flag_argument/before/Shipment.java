package refactoring._15_remove_flag_argument.before;

import java.time.LocalDate;

// Practice
// Remove flag argument : delete isRush -> separated into methods

public class Shipment {

    public LocalDate deliveryDate(Order order, boolean isRush) {
        if (isRush) {
//            int deliveryTime = switch (order.getDeliveryState()) {
//                case "WA", "CA", "OR" -> 1;
//                case "TX", "NY", "FL" -> 2;
//                default -> 3;
//            };

            int deliveryTime = 0;
            switch (order.getDeliveryState()) {
                case "WA":
                case "CA":
                case "OR":
                    deliveryTime = 1;
                    break;
                case "TX":
                case "NY":
                case "FL":
                    deliveryTime = 2;
                    break;
                default:
                    deliveryTime = 3;
            }

            return order.getPlacedOn().plusDays(deliveryTime);
        } else {
//            int deliveryTime = switch (order.getDeliveryState()) {
//                case "WA", "CA" -> 2;
//                case "OR", "TX", "NY" -> 3;
//                default -> 4;
//            };

            int deliveryTime = 0;
            switch (order.getDeliveryState()) {
                case "WA":
                case "CA":
                    deliveryTime = 2;
                    break;
                case "OR":
                case "TX":
                case "NY":
                    deliveryTime = 3;
                    break;
                default:
                    deliveryTime = 4;
            }

            return order.getPlacedOn().plusDays(deliveryTime);
        }
    }
}
