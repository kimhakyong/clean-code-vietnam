package basic_java.inheritance;

public class Salesman extends Employee {
    private int salesVolume;

    public Salesman(String name, int employeeId, String department, int salesVolume) {
        super(name, employeeId, department);
        this.salesVolume = salesVolume;
    }

    @Override
    public void printInformation() {
        System.out.println("** Salesman " + getName() + " ** : " + salesVolume);
    }

    @Override
    public int calculateSalary() {
        int basicSalary = 1500000;
        int bonus = salesVolume * 10000;

        return basicSalary + bonus;
    }

    // getter & setter 메서드
    public int getSalesVolume() {
        return salesVolume;
    }

    public void setSalesVolume(int salesVolume) {
        this.salesVolume = salesVolume;
    }
}
