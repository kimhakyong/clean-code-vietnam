package basic_java.inheritance;

// Practice
// 1. Rename Field : id --> employeeId
// 2. Change Function Declaration : doWork --> printInformation, calSalary --> calculateSalary
// 3. Pull Up Method : The workplace of all employees is at the head office.

public abstract class Employee {
    private String name;
    private int id;
    private String department;

    public Employee(String name, int id, String department) {
        this.name = name;
        this.id = id;
        this.department = department;
    }

    public abstract void printInformation();

    public abstract int calculateSalary();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getWorkPlace() {
        return "head office";
    }
}