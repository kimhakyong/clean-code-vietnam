package basic_java.inheritance;

public class Main {
    public static void main(String[] args) {
        Employee engineer = new Engineer("김철수", 1234, "개발팀", "Java");
        System.out.println("**엔지니어 " + engineer.getName() + "**");
        System.out.println("월급: " + engineer.calculateSalary() + "원");

        Employee manager = new Manager("박지영", 5678, "영업팀", "1팀");
        System.out.println("**관리자 " + manager.getName() + "**");
        System.out.println("월급: " + manager.calculateSalary() + "원");

        Employee salesman = new Salesman("이영희", 9012, "마케팅팀", 100);
        System.out.println("**영업사원 " + salesman.getName() + "**");
        System.out.println("월급: " + salesman.calculateSalary() + "원");
    }
}
