package basic_java.inheritance;

// Practice
// 1. Rename Variable : add -> bonus
// 2. Remove Dead Code
// 3. Inline Variable : calSalary() -> salary
// 4. Extract Variable : calSalary() -> salary

public class Engineer extends Employee {
    private String skillLevel;

    // 생성자
    public Engineer(String name, int employeeId, String department, String skillLevel) {
        super(name, employeeId, department);
        this.skillLevel = skillLevel;
    }

    @Override
    public void printInformation() {
        System.out.println("** Engineer " + getName() + " ** : " + skillLevel);
    }

    @Override
    public int calculateSalary() {
        int basicSalary = 1500000;
        int plus = 0;

        switch (skillLevel) {
            case "Java":
                plus = 700000;
                break;
            case "Python":
                plus = 600000;
                break;
            case "C++":
                plus = 500000;
                break;
            default:
                plus = 0;
        }

        return basicSalary + plus;
    }

    public String getSkillLevel() {
        return skillLevel;
    }

    public void setSkillLevel(String skillLevel) {
        this.skillLevel = skillLevel;
    }

}
