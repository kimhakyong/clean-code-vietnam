package basic_java.inheritance;

public class Manager extends Employee {
    private String team;

    // 생성자
    public Manager(String name, int employeeId, String department, String team) {
        super(name, employeeId, department);
        this.team = team;
    }

    // 상속받은 메서드 재정의 - 관리자의 업무 정의
    @Override
    public void printInformation() {
        System.out.println("** Manager " + getName() + " ** : " + team);
    }

    @Override
    public int calculateSalary() {
        int basicSalary = 1500000;
        int bonus = 0;

        if (this.team.equals("team1")) {
            bonus = 500000;
        } else if (this.team.equals("team2")) {
            bonus = 600000;
        } else {
            bonus = 300000;
        }

        return basicSalary + bonus;
    }

    // getter & setter 메서드
    public String getTeam() {
        return this.team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

}
