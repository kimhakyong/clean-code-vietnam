package basic_java._Optional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class Main {
    public static void main(String[] args) {
        Optional<String> optional = Optional.empty();

        try {
            System.out.println(optional.get());
        } catch (Exception e) {
            System.out.println(e);
        }
        System.out.println(optional.isPresent());

        optional = Optional.of("abcdef");

        System.out.println(optional.get());
        System.out.println(optional.isPresent());

        optional = Optional.ofNullable("John");
        System.out.println(optional.orElse("anonymous"));

        List<String> nameList = Optional.ofNullable(getNames())
                .orElseGet(() -> new ArrayList<String>());

        nameList = Optional.ofNullable(getNames())
                .orElseThrow(() -> new RuntimeException());
    }

    private static List<String> getNames() {
        return Arrays.asList(
                "John", "Mary", "David", "Sarah", "Michael",
                "Elizabeth", "Robert", "Jessica", "James", "Patricia"
        );
    }
}
