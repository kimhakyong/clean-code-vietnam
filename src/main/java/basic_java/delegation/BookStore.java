package basic_java.delegation;

import java.util.ArrayList;
import java.util.List;

// Practice
// Please change the form of delegation.
// 1. Rename Field : bookList -> books
// 2. Replace Loop with Pipeline
//    2.1 searchBooksByTitle() for loop --> lambda format
//    2.2 searchBookById() for loop --> lambda format
// 3. Create BookService Interface
// 4. Create BookServiceImpl Class
// 5. Move Function : BookStore method
// 6. Removing Null Return : getBookById Null Return --> Optional

public class BookStore {
    private List<Book> books;

    private BookService bookService;

    public BookStore(List<Book> books, BookService bookService) {
        this.books = books;
        this.bookService = bookService;
    }

    public BookStore() {
        books = new ArrayList<>();
    }

    public void addBook(Book book) {
        books.add(book);
    }

    public Book getBookById(int id) {
        return bookService.getBookById(id);
    }

    public List<Book> searchBooksByTitle(String title) {
        return bookService.searchBooksByTitle(title);
    }

    public void purchaseBook(Book book) {
        // 결제 처리
        // 재고 관리
    }
}