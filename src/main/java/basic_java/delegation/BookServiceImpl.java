package basic_java.delegation;

import java.util.List;
import java.util.stream.Collectors;

public class BookServiceImpl implements BookService {
    List<Book> books;

    public BookServiceImpl(List<Book> books) {
        this.books = books;
    }

    @Override
    public List<Book> searchBooksByTitle(String title) {
        List<Book> results = books.stream()
                .filter(book -> book.getTitle().contains(title))
                .collect(Collectors.toList());

        return results;
    }

    @Override
    public Book getBookById(int id) {
        Book foundBook = books.stream()
                .filter(book -> book.getId() == id)
                .findFirst()
                .orElse(null);

        return foundBook;
    }
}
