package basic_java.delegation;

import java.util.List;

public interface BookService {
    Book getBookById(int id);

    List<Book> searchBooksByTitle(String title);
}
