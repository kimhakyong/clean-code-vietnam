package basic_java.delegation;

public class Main {
    public static void main(String[] args) {
        BookStore bookStore = new BookStore();

        bookStore.addBook(new Book(1, "book1", 100, 5));
        bookStore.addBook(new Book(2, "book2", 200, 6));
        bookStore.addBook(new Book(3, "book3", 300, 7));
        bookStore.addBook(new Book(4, "book4", 400, 8));
        bookStore.addBook(new Book(5, "book5", 500, 9));

        System.out.println(bookStore.searchBooksByTitle("book1"));
    }
}
