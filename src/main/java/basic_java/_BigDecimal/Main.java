package basic_java._BigDecimal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

/**
 * BigDecimal은 Java 언어에서 숫자를 정밀하게 저장하고 표현할 수 있는 유일한 방법이다.
 * 소수점을 저장할 수 있는 가장 크기가 큰 타입인 double은 소수점의 정밀도에 있어 한계가 있어 값이 유실될 수 있다.
 * Java 언어에서 돈과 소수점을 다룬다면 BigDecimal은 선택이 아니라 필수이다.
 * BigDecimal의 유일한 단점은 느린 속도와 기본 타입보다 조금 불편한 사용법 뿐이다.
 */
public class Main {
    public static void main(String[] args) {
//        doubleErrorExample1();
//        doubleErrorExample2();
//        floatErrorExample();

//        basicConstant();
//        initinalization();
//        comparisonOperations();
//        fourBasicOperations();
//        decimalPointProcessing();
        divideHandling();

    }

    private static void divideHandling() {
        BigDecimal b10 = new BigDecimal("10");
        BigDecimal b3 = new BigDecimal("3");

        // 나누기 결과가 무한으로 떨어지면 예외 발생
        // java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result.
        try {
            b10.divide(b3);
        } catch (ArithmeticException e) {
            System.out.println(e);
        }

        // 반올림 정책을 명시하면 예외가 발생하지 않음
        // 3
        b10.divide(b3, RoundingMode.HALF_EVEN);

        // 반올림 자리값을 명시
        // 3.333333
        b10.divide(b3, 6, RoundingMode.HALF_EVEN);

        // 3.333333333
        b10.divide(b3, 9, RoundingMode.HALF_EVEN);

        // 전체 자리수를 7개로 제한하고 HALF_EVEN 반올림을 적용한다.
        // 3.333333
        b10.divide(b3, MathContext.DECIMAL32);

        // 전체 자리수를 16개로 제한하고 HALF_EVEN 반올림을 적용한다.
        // 3.333333333333333
        b10.divide(b3, MathContext.DECIMAL64);

        // 전체 자리수를 34개로 제한하고 HALF_EVEN 반올림을 적용한다.
        // 3.333333333333333333333333333333333
        b10.divide(b3, MathContext.DECIMAL128);

        // 전체 자리수를 제한하지 않는다.
        // java.lang.ArithmeticException: Non-terminating decimal expansion; no exact representable decimal result. 예외가 발생한다.
        try {
            b10.divide(b3, MathContext.UNLIMITED);
        } catch (ArithmeticException e) {
            System.out.println(e);
        }
    }

    private static void decimalPointProcessing() {
        // 소수점 이하를 절사한다.
        // 1
        new BigDecimal("1.1234567890").setScale(0, RoundingMode.FLOOR);

        // 소수점 이하를 절사하고 1을 증가시킨다.
        // 2
        new BigDecimal("1.1234567890").setScale(0, RoundingMode.CEILING);

        // 음수에서는 소수점 이하만 절사한다.
        // -1
        new BigDecimal("-1.1234567890").setScale(0, RoundingMode.CEILING);

        // 소수점 자리수에서 오른쪽의 0 부분을 제거한 값을 반환한다.
        // 0.9999
        new BigDecimal("0.99990").stripTrailingZeros();

        // 소수점 자리수를 재정의한다.
        // 원래 소수점 자리수보다 작은 자리수의 소수점을 설정하면 예외가 발생한다.
        // java.lang.ArithmeticException: Rounding necessary
        try {
            new BigDecimal("0.1234").setScale(3);
        } catch (ArithmeticException e) {
            System.out.println(e);
        }

        // 반올림 정책을 명시하면 예외가 발생하지 않는다.
        // 0.123
        new BigDecimal("0.1234").setScale(3, RoundingMode.HALF_EVEN);

        // 소수점을 남기지 않고 반올림한다.
        // 0
        new BigDecimal("0.1234").setScale(0, RoundingMode.HALF_EVEN);
        // 1
        new BigDecimal("0.9876").setScale(0, RoundingMode.HALF_EVEN);
    }

    private static void fourBasicOperations() {
        BigDecimal value1 = new BigDecimal("10");
        BigDecimal value2 = new BigDecimal("3");

        System.out.println(value1.add(value2));
        System.out.println(value1.subtract(value2));
        System.out.println(value1.multiply(value2));
        try {
            System.out.println(value1.divide(value2));
        } catch (ArithmeticException e) {
            System.out.println(e);
        }
        System.out.println(value1.divide(value2, 3, RoundingMode.HALF_EVEN));

        // 전체 자리수를 34개로 제한
        System.out.println(value1.remainder(value2, MathContext.DECIMAL128));

        System.out.println(new BigDecimal("-3").abs());
        System.out.println(value1.min(value2));
        System.out.println(value1.max(value2));
    }

    private static void comparisonOperations() {
        BigDecimal value1 = BigDecimal.valueOf(2.01);
        BigDecimal value2 = BigDecimal.valueOf(2.010);

        System.out.println(value1 == value2);
        System.out.println(value1.equals(value2));
        System.out.println(value1.compareTo(value2));
    }

    private static void initinalization() {
        new BigDecimal(0.01);
        new BigDecimal("0.01");
        BigDecimal.valueOf(0.01);
    }

    private static void basicConstant() {
        System.out.println(BigDecimal.ZERO);
        System.out.println(BigDecimal.ONE);
        System.out.println(BigDecimal.TEN);
    }

    private static void doubleErrorExample1() {
        double a = 10.0000;
        double b = 3.0000;

        // 기대값: 13
        // 실제값: 13.000001999999999
        System.out.println(a + b);

        // 기대값: 7
        // 실제값: 6.999999999999999
        System.out.println(a - b);

        // 기대값: 30
        // 실제값: 30.000013000000997
        System.out.println(a * b);

        // 기대값: 3.33333...
        // 실제값: 3.333332555555814
        System.out.println(a / b);
    }

    private static void doubleErrorExample2() {
        double doubleValue = 0.0d;

        for (int i = 0; i < 100; i++) {
            doubleValue += 0.1d;
        }

        double doubleResult = 0.1d * 100;

        System.out.println("expected value is : " + doubleResult + " result is : " + doubleValue);
    }

    private static void floatErrorExample() {
        float floatValue = 0.0f;

        for (int i = 0; i < 100; i++) {
            floatValue += 0.1f;
        }

        float floatResult = 0.1f * 100;
        System.out.println("expected value is : " + floatResult + " result is : " + floatValue);
    }
}
