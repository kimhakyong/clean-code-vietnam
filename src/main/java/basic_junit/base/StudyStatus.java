package basic_junit.base;

public enum StudyStatus {
    DRAFT, STARTED, ENDED
}
