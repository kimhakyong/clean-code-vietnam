package basic_junit.base;

public class Study {
    private StudyStatus status;

    private int limit;

    public Study(StudyStatus status, int limit) {
        if (limit <= 0)
            throw new IllegalArgumentException("The limit value must be greater than 0.");

        this.status = status;
        this.limit = limit;
    }

    public StudyStatus getStatus() {
        return this.status;
    }

    public int getLimit() {
        return limit;
    }
}
