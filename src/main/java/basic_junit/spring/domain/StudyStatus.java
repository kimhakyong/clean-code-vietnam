package basic_junit.spring.domain;

public enum StudyStatus {
    DRAFT, OPENED, STARTED, ENDED
}
