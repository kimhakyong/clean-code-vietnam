package basic_junit.spring.member;

import basic_junit.spring.domain.Member;
import basic_junit.spring.domain.Study;

import java.util.Optional;

public interface MemberService {

    Optional<Member> findById(Long memberId);

    void validate(Long memberId);

    void notify(Study newstudy);

    void notify(Member member);
}
